package main

import (
	"github.com/gorilla/mux"
)

const (
	shortenPath = "/shorten"
)

func (s *server) routes() {
	s.mux = mux.NewRouter()
	s.mux.HandleFunc(shortenPath, s.handleShorten).Methods("POST")
	s.mux.HandleFunc("/{shortcode}", s.handleShortcode).Methods("GET")
	s.mux.HandleFunc("/{shortcode}/stats", s.handleShortcodeStats).Methods("GET")
}
