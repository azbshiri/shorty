package main

import "net/http"

type Err struct {
	ErrorHuman string `json:"error_human"`
	ErrorCode  int    `json:"error_code"`
}

var (
	// InvalidJSONError represents invalid json format.
	InvalidJSONError = Err{
		ErrorHuman: "Invalid JSON format",
		ErrorCode:  http.StatusBadRequest}

	// InvalidJSONError represents invalid shortcode format.
	InvalidShortcodeFormat = Err{
		ErrorHuman: "The shortcode fails to meet the following regexp: " + ShortcodeRegex,
		ErrorCode:  http.StatusBadRequest}

	// MissingURL represents missing url.
	MissingURL = Err{
		ErrorHuman: "URL is not present",
		ErrorCode:  http.StatusBadRequest}

	// UnavailableDB represents database availability issue.
	UnavailableDB = Err{
		ErrorHuman: "Sorry, it's seems the database is not available at the moment",
		ErrorCode:  http.StatusServiceUnavailable}

	// UnavailableDB represents database availability issue.
	UsedShortcode = Err{
		ErrorHuman: "The the desired shortcode is already in use. Shortcodes are case-sensitive.",
		ErrorCode:  http.StatusServiceUnavailable}

	ShortcodeNotFound = Err{
		ErrorHuman: "The shortcode cannot be found in the system",
		ErrorCode:  http.StatusNotFound}
)
