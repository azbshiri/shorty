#!/usr/bin/env bash

curl localhost:27017 &>/dev/null
if test "$?" != "0"; then
  echo "running mongo..."
  nohup mongod &>/dev/null &
fi

echo "getting dependencies..."
go get ./...

echo "running tests..."
go test -v .
