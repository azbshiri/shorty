FROM golang:1.9-alpine

RUN apk add --no-cache git

RUN mkdir -p /go/src/github.com/azbshiri/shorty
WORKDIR /go/src/github.com/azbshiri/shorty
COPY . ./

RUN go get ./...
RUN go build
