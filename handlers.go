package main

import (
	"encoding/json"
	"net/http"
	"regexp"
	"time"

	"github.com/gorilla/mux"
	"github.com/lucasjones/reggen"
)

// H represents a hash map.
type H map[string]interface{}

// Shorty represents shorty serivce business model.
type Shorty struct {
	LastSeenDate  *time.Time `json:"last_seen_date" bson:"last_seen_date"`
	Shortcode     string     `json:"shortcode" bson:"shortcode"`
	StartDate     time.Time  `json:"start_date" bson:"start_date"`
	URL           string     `json:"url" bson:"url"`
	RedirectCount int        `json:"redirect_count" bson:"redirect_count"`
}

const (
	// ShortcodeRegex represents shortcode regexp.
	ShortcodeRegex = "^[0-9a-zA-Z_]{6}$"
)

// generateShortcode generates a 6 length shortcode based on ShortcodeRegex.
func generateShortcode() (str string) {
	str, _ = reggen.Generate(ShortcodeRegex, 6)
	return
}

func (s *server) handleShorten(w http.ResponseWriter, r *http.Request) {
	var err error
	var shorty Shorty

	err = json.NewDecoder(r.Body).Decode(&shorty)
	if err != nil {
		body, _ := json.Marshal(InvalidJSONError)
		w.WriteHeader(http.StatusBadRequest)
		w.Write(body)
		return
	}

	if shorty.URL == "" {
		body, _ := json.Marshal(MissingURL)
		w.WriteHeader(http.StatusBadRequest)
		w.Write(body)
		return
	}

	if shorty.Shortcode != "" {
		ok, _ := regexp.MatchString(ShortcodeRegex, shorty.Shortcode)
		if !ok {
			body, _ := json.Marshal(InvalidShortcodeFormat)
			w.WriteHeader(http.StatusBadRequest)
			w.Write(body)
			return
		}

		err := s.find(shorty.Shortcode, &shorty)
		if err == nil {
			body, _ := json.Marshal(UsedShortcode)
			w.WriteHeader(http.StatusConflict)
			w.Write(body)
			return
		}
	} else {
		shorty.Shortcode = generateShortcode()
	}

	err = s.create(&shorty)
	if err != nil {
		body, _ := json.Marshal(UnavailableDB)
		w.WriteHeader(http.StatusServiceUnavailable)
		w.Write(body)
		return
	}

	body, _ := json.Marshal(H{"shortcode": shorty.Shortcode})
	w.WriteHeader(http.StatusCreated)
	w.Write(body)
	return
}

func (s *server) handleShortcode(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var err error

	ok, _ := regexp.MatchString(ShortcodeRegex, vars["shortcode"])
	if !ok {
		body, _ := json.Marshal(InvalidShortcodeFormat)
		w.WriteHeader(http.StatusBadRequest)
		w.Write(body)
		return
	}

	var shorty Shorty
	err = s.find(vars["shortcode"], &shorty)
	if err != nil {
		body, _ := json.Marshal(ShortcodeNotFound)
		w.WriteHeader(http.StatusNotFound)
		w.Write(body)
		return
	}

	err = s.incrementCounter(&shorty)
	if err != nil {
		body, _ := json.Marshal(UnavailableDB)
		w.WriteHeader(http.StatusServiceUnavailable)
		w.Write(body)
		return
	}

	http.Redirect(w, r, shorty.URL, http.StatusFound)
}

func (s *server) handleShortcodeStats(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var err error

	ok, _ := regexp.MatchString(ShortcodeRegex, vars["shortcode"])
	if !ok {
		body, _ := json.Marshal(InvalidShortcodeFormat)
		w.WriteHeader(http.StatusBadRequest)
		w.Write(body)
		return
	}

	var shorty Shorty
	err = s.find(vars["shortcode"], &shorty)
	if err != nil {
		body, _ := json.Marshal(ShortcodeNotFound)
		w.WriteHeader(http.StatusNotFound)
		w.Write(body)
		return
	}

	h := make(H)
	h["start_date"] = shorty.StartDate
	h["redirect_count"] = shorty.RedirectCount
	if shorty.RedirectCount != 0 {
		h["last_seen_date"] = shorty.LastSeenDate
	}

	body, _ := json.Marshal(h)
	w.WriteHeader(http.StatusOK)
	w.Write(body)
}
