package main

import (
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

// find finds a shorty based on shortcode and populates the given shorty.
func (s *server) find(shortcode string, shorty *Shorty) (err error) {
	err = s.DB().
		Find(bson.M{"shortcode": shortcode}).
		One(shorty)
	return
}

// create creates a shorty based on the given shorty.
func (s *server) create(shorty *Shorty) (err error) {
	shorty.StartDate = time.Now()
	err = s.DB().Insert(shorty)
	return
}

// incrementCounter increments redirect_count and updates last_seen_date and
// poulates a new shorty.
func (s *server) incrementCounter(shorty *Shorty) (err error) {
	chg := mgo.Change{
		Update: bson.M{
			"$inc": bson.M{"redirect_count": 1},
			"$set": bson.M{"last_seen_date": time.Now()}},
		ReturnNew: true,
	}
	_, err = s.DB().
		Find(bson.M{"shortcode": shorty.Shortcode}).
		Apply(chg, shorty)
	return
}
