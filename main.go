package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"github.com/globalsign/mgo"
	"github.com/gorilla/handlers"
)

func main() {
	ses, err := mgo.DialWithTimeout(os.Getenv("MONGO_URL"), 5*time.Second)
	if err != nil {
		log.Fatal(err)
	}

	server := new(server)
	server.col = ses.DB("shorty").C("shorties")
	withRecovery := handlers.RecoveryHandler()(server)
	withLogging := handlers.LoggingHandler(os.Stdout, withRecovery)
	http.ListenAndServe(":3000", withLogging)
}
