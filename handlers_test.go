package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/globalsign/mgo"
	"github.com/stretchr/testify/assert"
)

var (
	testServer *server
	shorty     = Shorty{
		Shortcode:     "12used",
		URL:           "https://google.com",
		RedirectCount: 0,
		LastSeenDate:  nil,
	}
	secondVisitShorty = Shorty{
		Shortcode:     "secon2",
		URL:           "https://google.com",
		RedirectCount: 2,
		LastSeenDate:  &[]time.Time{time.Now()}[0],
	}
)

func setup() {
	mutex := &sync.Mutex{}
	mutex.Lock()
	testServer.create(&shorty)
	testServer.create(&secondVisitShorty)
	mutex.Unlock()
}

func teardown() {
	mutex := &sync.Mutex{}
	mutex.Lock()
	testServer.col.DropCollection()
	mutex.Unlock()
}

func TestMain(m *testing.M) {
	ses, err := mgo.DialWithTimeout("localhost:27017", 5*time.Second)
	if err != nil {
		log.Fatal(err)
	}

	testServer = new(server)
	testServer.col = ses.DB("shorty_test").C("shorties")
	os.Exit(m.Run())
}

func TestGenerateShortcode_ValidFormat(t *testing.T) {
	shortcode := generateShortcode()
	assert.Regexp(t, ShortcodeRegex, shortcode)
}

func TestShorten_InvalidJSON(t *testing.T) {
	var body Err
	data := strings.NewReader(``)

	res, err := doRequest(testServer, "POST", shortenPath, data)

	json.NewDecoder(res.Body).Decode(&body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusBadRequest, res.Code)
	assert.Equal(t, InvalidJSONError, body)
}

func TestShorten_MissingURL(t *testing.T) {
	var body Err
	data := strings.NewReader(`{}`)

	res, err := doRequest(testServer, "POST", shortenPath, data)

	json.NewDecoder(res.Body).Decode(&body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusBadRequest, res.Code)
	assert.Equal(t, MissingURL, body)
}

func TestShorten_InvalidShortcodeFormat(t *testing.T) {
	var body Err
	data := strings.NewReader(`{"url": "https://google.com", "shortcode": "bad"}`)

	res, err := doRequest(testServer, "POST", shortenPath, data)

	json.NewDecoder(res.Body).Decode(&body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusBadRequest, res.Code)
	assert.Equal(t, InvalidShortcodeFormat, body)
}

func TestShorten_UsedShortcode(t *testing.T) {
	setup()
	var body Err
	data := strings.NewReader(`{"url": "https://google.com", "shortcode": "12used"}`)

	res, err := doRequest(testServer, "POST", shortenPath, data)

	json.NewDecoder(res.Body).Decode(&body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusConflict, res.Code)
	assert.Equal(t, UsedShortcode, body)

	teardown()
}

func TestShorten_CreatedWithShortcode(t *testing.T) {
	var body H
	data := strings.NewReader(`{"url": "https://google.com", "shortcode": "123new"}`)

	res, err := doRequest(testServer, "POST", shortenPath, data)

	json.NewDecoder(res.Body).Decode(&body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, res.Code)
	assert.Equal(t, H{"shortcode": "123new"}, body)

	teardown()
}

func TestShorten_CreatedWithoutShortcode(t *testing.T) {
	var body H
	data := strings.NewReader(`{"url": "https://google.com"}`)

	res, err := doRequest(testServer, "POST", shortenPath, data)

	json.NewDecoder(res.Body).Decode(&body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, res.Code)
	assert.Len(t, body["shortcode"].(string), 6)

	teardown()
}

func TestShortcode_NotFound(t *testing.T) {
	var body Err

	res, err := doRequest(testServer, "GET", "/"+"not123", nil)

	json.NewDecoder(res.Body).Decode(&body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNotFound, res.Code)
	assert.Equal(t, ShortcodeNotFound, body)
}

func TestShortcode_InvalidShortcodeFormat(t *testing.T) {
	var body Err

	res, err := doRequest(testServer, "GET", "/"+"bad", nil)

	json.NewDecoder(res.Body).Decode(&body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusBadRequest, res.Code)
	assert.Equal(t, InvalidShortcodeFormat, body)
}

func TestShortcode_OK(t *testing.T) {
	setup()

	res, err := doRequest(testServer, "GET", "/"+shorty.Shortcode, nil)

	assert.NoError(t, err)
	assert.Equal(t, http.StatusFound, res.Code)
	assert.Equal(t, "https://google.com", res.Header().Get("Location"))

	teardown()
}

func TestShortcodeStats_InvalidShortcodeFormat(t *testing.T) {
	var body Err

	res, err := doRequest(testServer, "GET", "/"+"bad"+"/stats", nil)

	json.NewDecoder(res.Body).Decode(&body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusBadRequest, res.Code)
	assert.Equal(t, InvalidShortcodeFormat, body)
}

func TestShortcodeStats_NotFound(t *testing.T) {
	var body Err

	res, err := doRequest(testServer, "GET", "/"+"not123"+"/stats", nil)

	json.NewDecoder(res.Body).Decode(&body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNotFound, res.Code)
	assert.Equal(t, ShortcodeNotFound, body)
}

func TestShortcodeStats_FirstVisit(t *testing.T) {
	setup()
	var body struct {
		StartDate     time.Time  `json:"start_date"`
		LastSeenDate  *time.Time `json:"last_seen_date"`
		RedirectCount int        `json:"redirect_count"`
	}

	res, err := doRequest(testServer, "GET", "/"+shorty.Shortcode+"/stats", nil)

	json.NewDecoder(res.Body).Decode(&body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, 0, body.RedirectCount)
	assert.Nil(t, body.LastSeenDate)

	teardown()
}

func TestShortcodeStats_SecondVisit(t *testing.T) {
	setup()
	var body struct {
		StartDate     time.Time  `json:"start_date"`
		LastSeenDate  *time.Time `json:"last_seen_date"`
		RedirectCount int        `json:"redirect_count"`
	}

	res, err := doRequest(testServer, "GET",
		"/"+secondVisitShorty.Shortcode+"/stats", nil)

	json.NewDecoder(res.Body).Decode(&body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, 2, body.RedirectCount)
	assert.NotNil(t, body.LastSeenDate)

	teardown()
}

func doRequest(handler http.Handler, method, path string, body io.Reader) (*httptest.ResponseRecorder, error) {
	req, err := http.NewRequest(method, path, body)
	if err != nil {
		return nil, err
	}
	res := httptest.NewRecorder()
	handler.ServeHTTP(res, req)
	return res, nil
}
