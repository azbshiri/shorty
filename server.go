package main

import (
	"net/http"

	"github.com/globalsign/mgo"
	"github.com/gorilla/mux"
)

// server represents shorty bounded context.
type server struct {
	col *mgo.Collection
	mux *mux.Router
}

// DB returns MongoDB collection.
func (s *server) DB() *mgo.Collection {
	return s.col
}

// ServeHTTP makes server a http.Handler compatible.
func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	s.routes()
	s.mux.ServeHTTP(w, r)
}
